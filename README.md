# DHT

É uma estrutura dispersa usada em redes [peer-to-peer](http://www.inf.ufsc.br/~frank.siqueira/INE5418/2.3.P2P-Folhetos.pdf), nela é possível alocar ou obter informações de forma decentralizada, escalável e com boa performance.
Essa estrutura é organizada em nós, onde cada um deles é responsável por manter parte de um mapeamento de chaves, formando num todo um [hash table](https://pt.wikipedia.org/wiki/Tabela_de_dispers%C3%A3o) distribuído, esta estrutura foi modelada de tal forma que qualquer mudança na [cardinalidade](https://pt.wikipedia.org/wiki/Cardinalidade) do conjunto dos nós cause o mínimo de desordem.
 
#### 1.1 Mapeamento

Conforme mencionado acima cada nó mantém parte do mapeamento, ou seja cada nó possui um identificador único, de tal forma que um nó com ID i conhece todas as chaves onde o id é mais próximo. Essa proximidade é definida de forma arbitrária através de uma função sigma(k1,k2). Supondo que a função sigma(k1,k2) define como próximo a subtração entre o id de k2 pelo k1 menor ou igual 50, temos como exemplo a imagem abaixo que demonstra um conjunto de nós com cardinalidade k. Perceba na imagem que cada peer conhece até 50 outras chaves (demonstrado pelas bolas).
![esquema01](./images/SD1.png)

Essa implementação se baseia na ideia de [hash consistente](https://en.wikipedia.org/wiki/Consistent_hashing), que possui uma propriedade essencial de modificar somente nós com ID adjacentes na ocorrência de uma adição ou remoção de um determinado nó, assim nenhum nó restante é afetado, contribuindo para o mínimo de desordem nesse tipo de operação. Nesse contexto o hash consistente atribui outra vantagem, que é o menor uso de banda, uma vez que não é necessário reorganizar todas as chaves do conjunto.

#### 1.1.1 Inserção ou remoção

Supondo que no conjunto proposto na figura acima em um determinado momento seja inserido um peer com id 180, para que tal operação seja possível, primeiro localiza se o ponto onde esse peer deverá ficar (procedimento feito através do envio lookup(k) explicado posteriormente), no caso desse conjunto é entre os nós 151 e 200. Encontrado o ponto, é feito uma atualização dos nós adjacentes ao ponto em questão, assim o peer novato de id 180 passa a conhecer o intervalo 151 a 179 e o peer com ID 200 após a operação conhece as chaves 181 a 200, conforme a imagem abaixo:
![esquema02](./images/SD1%20-%202.png)
Após a inserção é feita uma atualização dos links para que a rede saiba do peer novato, de maneira análoga é feita a remoção.

#### 2 Lookup

Lookup é como é denominado a operação de busca entre os nós por uma determinada chave k, quando é preciso fazer uma inserção ou remoção de um determinado valor, primeiro é calculado sua chave, com a chave obtida é enviado o lookup por toda a DHT afim de encontrar o nó responsável pela mesma. Cada nó possui um conjunto de links de seus vizinhos, e quando o lookup é enviado ele é encaminhado de nó a nó até seja encontrado o nó correspondente a chave k. A vizinhança é definida com base na função sigma defina no começo do artigo, e através da vizinhança é possível encaminhar uma mensagem para qualquer nó através do [algoritmo guloso](https://pt.wikipedia.org/wiki/Algoritmo_guloso). Assim quando não houver mais vizinhos é sinal que chegou ao nó responsável pela chave.
Existem duas modalidades de lookup:
1) HIP NAME: dado uma chave a operação de lookup retorna um identificador.
2) HIP ADDRESS: dado um identificador a operação retorna um endereço de IP.

Softwares como uTorrent fazem uso dos dois tipos, hip name para localizar um conteúdo e o hip address para saber as nacionalidades de seus pares através do ip.

#### Características:

1) Autonomia e descentralização, onde os pares participantes formam um sistema,
   sem conhecer de fato quem está coordenando.
2) Tolerância a falhas,o sistema pode corrigir continuamente juntando, excluindo e
   substituindo pares
3) Escalabilidade, já que o sistema pode adicionar os hashs de acordo com o
   crescimento permitindo milhões de pares.

#### Demonstraçao

##### requisitos:
1) nodejs instalado
2) depois de instalado o nodejs clone o projeto ```git clone https://gitlab.com/danielnd14/trabalho-sd1.git```
3) rode o comando ```npm install```

##### Run
Execute o comando ```nodejs app.js```

_será exibido no console o hash para o link magnético inserido dentro do código, e tambem será exibido todos os pares que encontram esse arquivo._

OBS: você é livre para alterar o código e inserir outros links magnéticos para testar.

#### Considerações finais

DHT é uma estrutura que distribui conteúdos na forma chave-valor através de redes peer-to-peer, com isso obtém-se um alto nível de escalabilidade e a alta autonomia e por consequência uma adaptabilidade, isso faz com que esse tipo de protocolo seja bastante aproveitado em downloads de BitTorrent, sistemas de arquivos distribuídos, web caching e outros. A escalabilidade e adaptabilidade se dá devido ao fato do uso de hash consistente, o que impede gargalos de entrada e saída de dados em cada nó, visto que operações corriqueiras como inserção ou remoção de nó não envolve uma reorganização de toda a rede, por isso DHT é considerada uma rede de alta performance, pois suporta milhões de pares.

#### Referencias:

https://www.ime.usp.br/~vmoreira/DHT.html

https://pt.wikipedia.org/wiki/Distributed_hash_table#Aplica%C3%A7%C3%B5es_usando_DHTs

https://pt.wikipedia.org/wiki/Algoritmo_guloso

https://pt.wikipedia.org/wiki/Cardinalidade

https://tools.ietf.org/html/rfc6537#section-4

https://en.wikipedia.org/wiki/Consistent_hashing

http://www.inf.ufsc.br/~frank.siqueira/INE5418/2.3.P2P-Folhetos.pdf

https://www.npmjs.com/package/bittorrent-dht

https://gitlab.com/jeanjonatas/dhttutorial

OBS: Todos os links foram acessados em 20/12/2018 as 23:59. E todas as imagens contidas nesse artigo foram de autoria própria.


