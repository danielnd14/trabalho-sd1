// codigo forked from https://gitlab.com/jeanjonatas/dhttutorial e alterado para mostrar todos os pares de um determinado link magnetico
let port = 8081;
let DHT = require('bittorrent-dht');
let magnet = require('magnet-uri');


let red, blue, reset;
red = '\u001b[31m';
blue = '\u001b[34m';
reset = '\u001b[0m';


let uri = 'magnet:?xt=urn:btih:f5b31b1bd67bf65fe97be298ec7c473cb2e3e201&dn=elementaryos-5.0-stable.20181016.iso&tr=https%3A%2F%2Fashrise.com%3A443%2Fphoenix%2Fannounce&tr=udp%3A%2F%2Fopen.demonii.com%3A1337%2Fannounce&tr=udp%3A%2F%2Ftracker.ccc.de%3A80%2Fannounce&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80%2Fannounce&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80%2Fannounce&ws=http://sfo1.dl.elementary.io/download/MTU0NTM1Nzc4MQ==/elementaryos-5.0-stable.20181016.iso';
let parsed = magnet(uri);

function informarHash() {
	console.log('Informação do HASH:');
	console.log(red + parsed.infoHash);
}

informarHash();

let dht = new DHT();


dht.listen(port, function () {
	console.log('Rodando na porta ' + port);
});

dht.lookup(parsed.infoHash); //funçao lookup do tipo HIP NAME

dht.on('peer', function (peer, infoHash, from) {
	console.log(blue + 'Encontrando par ' + peer.host + ':' + peer.port + ' through ' + from.address + ':' + from.port)
});

console.log(reset);
